$(document).ready(function() {
	// Create an array of styles.
	// Create an array of styles.
	var styles = [
	{
		featureType: "water",
		elementType: "all",
		stylers: [
			{ color: '#ffffff' },
			{ visibility: "on" },
			{ url : 'images/mapDot.jpg'}
		]
	},

	{
		featureType: "road",
		stylers: [
			{ "visibility": "off" }
		]
	},

	{
		featureType: "transit",
		stylers: [
			{ "visibility": "off" }
		]
	},

	{
		featureType: "administrative",
		stylers: [
			{ "visibility": "off" }
		]
	},

	{
		featureType: "landscape",
		elementType: "all",
		stylers: [
			{ "color": "#ff0000" }
		]
	},

	{
		featureType: "poi",
		stylers: [
			{ "color": "#ff0000" }
		]
	},
	{
		elementType: "labels",
		stylers: [
			{ "visibility": "off" }
		]
	}
	];

	var mapOptions = {
		center: { lat: 34.030034, lng: -45.068359},
		zoom: 3,
		minZoom :3,
		disableDefaultUI:true
	};
	
	var map = new google.maps.Map(document.getElementById('locMap'), mapOptions);
	map.setOptions({styles: styles});
});
