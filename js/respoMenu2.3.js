/* * *****************************************************************************
 *                JQUERY CODE FOR RESPONSIVE NAVIGATION
 * ******************************************************************************
 *      Author:     Ramesh Kumawat
 *      Email:      ramesh.kumawat@edynamic.net
 *      Website:    http://www.edynamic.net
 * 		Date Created 24 Nov 2014
 *      File:       respoMenu2.3.js
 *      Version:    2.3
 * ******************************************************************************
 *  VERION HISTORY:
 *                  1.1, 2.1, 2.2
 *
 * ******************************************************************************
 *  DESCRIPTION:
 *      This is a "Beta" Version2.3 of Responsive Naviation
 * ******************************************************************************
 * EXAMPLE USAGE
 *		$('#ID').respoMenu();
 *		$('.class').respoMenu();
 ********************************************************************************
 * PROPERTIES
 *      bgColor
 *      width
 *		position
 *      navBtnId
 *      close
 *      overlay
 *      effect
 *      respoDisplay
 * ******************************************************************************
 * Launch in new versions
 *		Multilevels will be creating
 *      Fixing bugs and issue 2.2 version
 * 
 * *******************************************************************************/
 
 (function($){
	$.fn.respoMenu = function(options){
		var settings = $.extend({
			/*added in version 2.1*/
			bgColor : '#333',
			width:250,
			position: 'left',
			
			/*added in version 2.2*/
			navBtnId : 'navBtn',
			close : false,
			
			/*added in version 2.3*/
			overlay : false,
			effect : 'overlap',  /*push ,overlap*/
			respoDisplay : 1000
			
		}, options);
		var test = false;
		var self = this;
		var navHTML = self.html();
		
		//Create responsive structure
		$('body').prepend('<div class="responsiveNav"><div class="innerNavBox"></div></div>');
		$('body').append('<div class="navOverlay"></div>');
		
		if(settings.close ==true){
			$('.innerNavBox').append('<div class="resNavClose" style="text-align:'+settings.position+'"><a href="javascript:void(0);">X</a></div>');
		}
		
		if(settings.overlay == true){
			$('.navOverlay') .css('background','rgba(36,61,91,0.7)');
		}
		
		//responsive nav setting passing
		
		var navTotalWidth;
		function stylSettings(){
			if($(window) .width()>760){
				$('.responsiveNav') .css({
					'backgroundColor' : settings.bgColor,
					'width' : 720
				});
				$('.responsiveNav') .css(settings.position, -720);
				navTotalWidth = 720;
				$('.responsiveNav') .addClass('tabletRespNav');
				
				
			}else{
				$('.responsiveNav') .css({
					'backgroundColor' : settings.bgColor,
					'width' : settings.width
				});
				$('.responsiveNav') .css(settings.position, -settings.width);				
				navTotalWidth = settings.width;
				$('.responsiveNav') .removeClass('tabletRespNav');
			}
		}
		stylSettings();
		
		//responsive nav content 
		$('.responsiveNav .innerNavBox') .append(navHTML);
		
		//Menu Button Click
		$('#'+ settings.navBtnId) .click(function(){
			if(test==false){
				openResNav();
				return false;
			}
			else{
				closeResNav();
				return false;
			}
		});
		
		$('.resNavClose') .add('.navOverlay') .click(closeResNav);
		$('#wrapper').click(closeResNav);
		
		//Function screen partitions
		function respoPartition(){
			if($(window).width()>settings.respoDisplay){
				closeResNav();
				$('.responsiveNav').hide();
				$('#'+ settings.navBtnId).hide();
				$(self).show();
			}else{
				$('.responsiveNav').show();
				$('#'+ settings.navBtnId).show();
				$(self).hide();
			}
		}
		respoPartition();
		$(window) .resize(respoPartition);
		
		//Functions Open and close
		function openResNav(){
			if(settings.effect == 'overlap'){
				if(settings.position == 'left'){
					$('.responsiveNav') .animate({ left:'0px'});
				}else{
					$('.responsiveNav') .animate({ right:'0px'});
				}
			}else if(settings.effect == 'push'){
				if(settings.position == 'left'){
					$('.responsiveNav') .animate({ left:'0px'});
					$('#wrapper').animate({left:settings.width});
				}else{
					$('.responsiveNav') .animate({ right:'0px'});
					$('#wrapper').animate({right:navTotalWidth});
				}
			}
			$('.resOpenBtn').hide();
			$('.resCloseBtn').css('display','inline-block');
			$('.navOverlay') .fadeIn();
			test = true;
		}
		
		function closeResNav(){
			if(settings.effect == 'overlap'){
				if(settings.position == 'left'){				
					$('.responsiveNav') .animate({left:-settings.width});		
				}else{	
					$('.responsiveNav') .animate({right:-settings.width});
				}
			}else if(settings.effect == 'push'){
				if(settings.position == 'left'){
					$('.responsiveNav') .animate({ left:-settings.width});
					$('#wrapper').animate({left:0});
				}else{
					$('.responsiveNav') .animate({right:-navTotalWidth});
					$('#wrapper').animate({right:0});
				}
			}
			$('.resOpenBtn').css('display','inline-block');
			$('.resCloseBtn').hide();
			$('.navOverlay') .fadeOut();
			test = false;
		}
		
		//on Resize window Responsive navigation set in Default case
		$(window) .resize(function(){
			if($(window) .width()>760){
				$('.responsiveNav') .css({
					'backgroundColor' : settings.bgColor,
					'width' : 720
				});
				navTotalWidth = 720;
				$('.responsiveNav') .addClass('tabletRespNav');
				
				if(test == true){
					$('#wrapper').css({right:720});
					$('.responsiveNav') .css({right:0});
				}else{
					$('#wrapper').css({right:0});
					$('.responsiveNav') .css({right:-720});
				}
				/*$('.responsiveNav') .css({right:-720});
				$('.resOpenBtn').show();
				$('.resCloseBtn').hide();
				$('.navOverlay') .fadeOut();
				test = false;
				return false;*/
				
			}else{
				$('.responsiveNav') .css({
					'backgroundColor' : settings.bgColor,
					'width' : settings.width
				});
				navTotalWidth = settings.width;
				$('.responsiveNav') .removeClass('tabletRespNav');
				
				if(test == true){
					$('#wrapper').css({right:settings.width});
					$('.responsiveNav') .css({right:0});
				}else{
					$('#wrapper').css({right:0});
					$('.responsiveNav') .css({right:-settings.width});
				}
				
				/*
				$('.responsiveNav') .css({right:-settings.width});
				$('.resOpenBtn').show();
				$('.resCloseBtn').hide();
				$('.navOverlay') .fadeOut();
				test = false;
				return false;*/
			}
			
		});
	}
}(jQuery));