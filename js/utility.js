 (function($){
$(document).ready(function() {

	/* css for popup 05-01-2015*/
    var hHig, txtContHig;
    hHig = $(document).height();
    $('.blueBgBtn').click(function () {
        txtContHig = $('.popupShow').height();
        $('.popShadow').css({ "height": hHig }).fadeIn(800);
        $('.popupShow').css('margin-top', -+txtContHig/2);
        $('.popupShow').fadeIn('slow');
        $('img.closeBtn').fadeIn('slow');
    });
    /* close popup below*/
    $('.popupShow .closeBtn').click(function () {
        $('.popupShow').fadeOut('slow');
        $('.popShadow').fadeOut('slow');
    });

    /* css for popup 28-02-2015*/
    var hhHig, txttttContHig;
    hhHig = $(document).height();
    $('.mailPopBtn').click(function () {
        txttttContHig = $('.popupShow').height();
        $('.popShadow').css({ "height": hhHig }).fadeIn(800);
        $('.popupShow').css('margin-top', -+txttttContHig / 2);
        $('.popupShow').fadeIn('slow');
        $('img.closeBtn').fadeIn('slow');
    });
    /* close popup below*/
    $('.popupShow .closeBtn').add('.forwardToMail').click(function () {
        $('.popupShow').fadeOut('slow');
        $('.popShadow').fadeOut('slow');
    });

	/*home page product slider */
	/*$(".productSlider").owlCarousel({
		items:4,
		slideBy:1,
		loop:true,
		mouseDrag:false,
		dots:false,
		nav:true,
		responsive:{	
			0:{items:1, slideBy:1},
			641:{items:2, slideBy:1},
			769:{items:3, slideBy:1},
			1270:{items:4, slideBy:1}
		}
	});
	*/
	/*custom select dropdown*/
	$('select').edyCustomSelectBox();
	
	/*mega dropdown menu function*/
	$('nav.navigation ul li').mouseenter(function() {
		var leftVal = ($(this) .offset().left + $(this).width())  - $(this).width()/2;
		$(this).find('.subMenu').css('backgroundPosition', leftVal+'px top').stop(true, true).delay(100) .fadeIn(200);
	}).mouseleave(function() {
		$(this) .find('.subMenu') .stop(true, true).fadeOut();
	});
	
	/*Sidebar Accordian - people page*/
	/*$('.sidebarAccordian h4') .click(function(){
		if($(this) .next('.accSideContent').is(':visible')){
			return false;
		}
		$('.accSideContent') .slideUp();
		$('.sidebarAccordian h4') .removeClass('openAcc');
		$(this) .addClass('openAcc');
		$(this) .next('.accSideContent') .slideDown();
	});*/
	
	/*Sidebar view more function*/
	$('.viewmoreList').click(function(){
		$(this).prev('ul').find('.hide').show();
	    $(this).next('.viewLessList').show();
		$(this).hide();
	});
	$('.viewLessList').click(function(){
		$(this).prevAll('ul:first').find('.hide').hide();
	    $(this).prev('.viewmoreList').show();
		$(this).hide();
	});

	/* arrow center for nav and heading */
	$('.midHeadNav ul li a').each(function(){
			midNavWid = $(this).width();
			$(this).find('b').width(midNavWid);
	});

	$('.midHeadNav h1').each(function(){
			midHeadWid = $(this).width();
			$(this).find('b').width(midHeadWid);
	});
	
	/*Search result page tabs*/
	$('.tabNav li a') .click(function(){
		var tabId = $(this) .attr('data-title');
		$('.tabNav li a') .removeClass('tabActive');
		$(this) .addClass('tabActive');
		$('.tabBoxes') .hide();
		$('.tabContent') .find(tabId) .show();
	});
	
	/*Other location dropdown*/
	$('#otherLocation') .click(function(){
		$(this).toggleClass('openedDrop');
		$('.otherLocContents').slideToggle();
	});
	
	
	/*header Search */
	$('body').append('<div class="headSearBlankOverlay"></div>');
	$('.searImgBtn').click(function(){
		$('.headSeaDropBox').toggle();
		$('.headSearBlankOverlay').toggle();
	});
	$(document).on('click','.headSearBlankOverlay', function(){
		$(this).add('.headSeaDropBox').hide();
	});
	
	$(document).on('click','.headSeaClBtn', function(){
		$('.headSeaDropBox').add('.headSearBlankOverlay').hide();
	});
	$(window).resize(function(){
		$('.headSearBlankOverlay').add('.headSeaDropBox').hide();
	});
	
	
	
	/*****************************
	Responsive Navigation
	******************************/
	$('.mobTabNav') .respoMenu({
		bgColor:'#fff',
		width:275,
		position:'right',
		navBtnId:'respoBtn',
		close:false,
		overlay:true,
		effect:'push', /*push , overlap*/
		respoDisplay: 1000
	});	
	
	/****************************
	END Responsive nav
	*****************************/

	/*08-01-2015 pg-our-insights-details.html */
	$('.recommndedPg div.group article:odd' ).css( "margin-right", "0" );
	$('.publications article:odd' ).css( "background-color", "#67798e" );
	
	/*People Detail - mobile content accordian*/
	$('.mobPeopAccBox h2').click(function(){
		$(this) .next('article:first').slideToggle();
		$(this) .toggleClass('openMobDetAcc');
	});
	
	
	/*People Listing show hide filter section*/
	$('.peoplefilterBtn').click(function(){
		$('.peoplSidebar').toggle();
	});
	$('.filterCancelBtn') .click(function(){
		$('.peoplSidebar').hide();
	});
	$('.optionChkFilters').click(function(){
		if($(window).width()>480){
			$('body, html').animate({scrollTop:100});
		}else{
			$('.peoplSidebar').hide();
		}
	});
	/*Reset function in people filter*/
	$('.resetAllFilter') .click(function(){
		$('.allRemove').trigger('click');
	});
	$(window).resize(function(){
		if($(window).width()>480){
			$('.peoplSidebar').show();
		}
	});
	
	
	/*Clear filters on desktop*/
	$('.clrFilterList a') .click(function(){
		$('.allRemove').trigger('click');
	});
	
	
	/*********************************************
	Location Page map Detail Popup
	**********************************************/
	/*$('.locMapBanner .mapDragBox').prepend('<div class="locPopBlankOverlay"></div>');*/
	$('.LocationPointer') .click(function(){
		if($(this).hasClass('mopt')){
			$(this).next('.locDetailBox').hide();
			$('.locPopBlankOverlay') .hide();
			$(this).removeClass('mopt');
		}else{
			$('.locDetailBox').hide();
			$(this).next('.locDetailBox').show();
			$('.locPopBlankOverlay') .show();
			$(this).addClass('mopt');
		}
	});
	$('.locCloseBtn') .click(function(){
		$('.locDetailBox').hide();
		$('.locPopBlankOverlay') .hide();
		$('.LocationPointer').removeClass('mopt');
		mapDefaultPos();
	});
	$('.locPopBlankOverlay').click(function(){
		$('.locDetailBox').hide();
		$('.locPopBlankOverlay') .hide();
		$('.LocationPointer').removeClass('mopt');
		mapDefaultPos();
	});
	
	function mapDefaultPos(){
		$('.mapDragBox').animate({
			left: -271,
			top: -355
		});
	}
	
	/**************************************
	Map move 
	***************************************/
	/*right top*/
	$('#location_london .LocationPointer, #location_berlin .LocationPointer').click(function(){
		$('.mapDragBox').animate({
			top : -92,
			left : -494
		});
	});
	
	/*Left top*/
	$('#location_portland .LocationPointer, #location_sanFrancisco .LocationPointer, #location_lasVegas .LocationPointer, #location_losAngeles .LocationPointer, #location_torrance .LocationPointer, #location_orangeCountry .LocationPointer, #location_sanDiego .LocationPointer, #location_tucson .LocationPointer, #location_phoenix .LocationPointer, #location_tucson .LocationPointer, #location_sanDiego .LocationPointer') .click(function(){
		$('.mapDragBox').animate({
			top : -212,
			left : -100
		});
	});
	
	/*center center*/
	$('#location_denver .LocationPointer, #location_minneapolis .LocationPointer, #location_milwa .LocationPointer, #location_chicago .LocationPointer, #location_indianapolis .LocationPointer, #location_cleverland .LocationPointer, #location_pittsburgh .LocationPointer, #location_philadelphia .LocationPointer, #location_washington .LocationPointer, #location_richmond .LocationPointer, #location_raleigh .LocationPointer, #location_atlanta .LocationPointer, #location_nashville .LocationPointer, #location_stLouis .LocationPointer, #location_kansas .LocationPointer, #location_boston .LocationPointer, #location_stamford .LocationPointer, #location_newYork .LocationPointer, #location_morristown .LocationPointer, #location_detroit .LocationPointer') .click(function(){
		$('.mapDragBox').animate({
			top : -217,
			left : -271
		});
	});	
	
	
	/*********************************************
	END Location Page map Detail Popup
	**********************************************/
	
	
	/*******************************************
	People media quote page
	********************************************/
	$('.mdQuoteContent ul li').each(function(index){
		if($(this).index()%3 ==0 && $(this).index()!=0 && $(this).index()%2 !=0){
			var i=0;
			while (i < 3) {
				var tt = $(this).index()+i;
				$('.mdQuoteContent ul li:eq('+tt+')').addClass('lightMediaBg');
				i++;
			}
		}
	});
	
	
	/******************************************
	go to top
	*******************************************/
	$('.goTop a').click(function(){
		$('html, body') .animate({'scrollTop':0});
	});
	$(window).scroll(function(){
		if($(this).scrollTop()>300){
			$('.goTop').fadeIn();
		}else{
			$('.goTop').fadeOut();
		}
	});
	
	/******************************************
	people Media Popup
	*******************************************/
	$('.leaSolPep').click(function(){
		$('html,body').animate({'scrollTop':0});
		var topVal = $(document).scrollTop();
		//$('.peopMCont_lightBox').css('top',topVal +100);
		$('.peopMCont_lightBox').css('top',100);
		$('.popOverlay').fadeIn();
		$(this).next('.peopMCont_lightBox') .fadeIn();
	});
	
	$('.learnCls').add('.popOverlay').click(function(){
		$('.popOverlay').add('.peopMCont_lightBox').fadeOut();
	});
	
	
	/*Mobile search e prop stop*/
	$( ".mbSearchBox input[type='text']" ).focus(function(e) {
		e.stopPropagation();
	});
	
	
	/*************************************************
	Close welcome Popup
	**************************************************/
	$('#welClBtn').click(function(){
		$('.welpopBox').hide();
		$('.welOverlay').fadeOut();
	});
	
	
	/*************************************************
	Close Message Popup
	**************************************************/
	$('.msgCloseOk').click(function(){
		$('.msgpopBox').hide();
		$('.msgOverlay').fadeOut();
	});
	
	
	/**************************************************
	People Detail View More and less
	***************************************************/
	$('.viewMore-peDet').click(function(){
		var extData = $(this).attr('data-title');
		$(this).hide();
		$(this).parent().find('.viewLess-peDet').show();
		$('#sum_'+extData).hide();
		$('#'+extData).show();
	});
	$('.viewLess-peDet').click(function(){
		var extData = $(this).attr('data-title');
		$(this).hide();
		$(this).parent().find('.viewMore-peDet').show();
		$('#'+extData).hide();
		$('#sum_'+extData).show();
	});	
	
	
	
	/***************************************************
	FAQs expand and collapse
	****************************************************/
	$('.expandable-header').click(function(){
		$(this).toggleClass('expanded');
		$(this).parent().find('.expandable-content').slideToggle();
	});
	
	$('.expAll').click(function(){
		$('.expandable-header').addClass('expanded');
		$('.expandable-content').slideDown()
	});
	$('.collAll').click(function(){
		$('.expandable-header').removeClass('expanded');
		$('.expandable-content').slideUp()
	});
	
	
	/***************************************************
	Alumini Question answer expand and collapse
	****************************************************/
	$('.al_expandable-header').click(function(){
		$(this).toggleClass('expanded');
		$(this).parent().find('.al_expandable-content').slideToggle();
	});
	
	
	/****************************************************
	print section start 
	*****************************************************/
    $(".print-head .container .left span").mouseover(function () {
        $(".filter-option").slideDown();
    });
    $(".print-head .container .left").mouseleave(function () {
        $(".filter-option").slideUp();
    });
	
	/*if ($(".print-extent-p").size() == 2) {
        $("input[value='print-extent-p']").show();
    }*/
    $('.filter-option input[type="checkbox"]').change(function () {

        var checkboxAttr = $(this).attr("value");
        if ($(this).is(":checked")) {
            $("." + checkboxAttr).show();
        } else {
            if ($("." + checkboxAttr).size() > 1) {
                $("." + checkboxAttr).not(":first").hide();
            } else {
                $("." + checkboxAttr).hide();
            }
        }
    });

    function optionalCheck(){
        $(".filter-option input").each(function () {
            var checkboxAttr = $(this).attr("value");
            if ($(this).is(":checked")) {
                $("." + checkboxAttr).show();
            } else {
                $("." + checkboxAttr).hide();
            }
        });
    }
    optionalCheck();
	
	
	/**************************************************
	Home page -our program height 
	***************************************************/
	$(window).load(heightSet);
    $(window).resize(function () {
		/*Home page Our program*/
        $('.homeProgram a.programBox').removeAttr('style');
		
		/*Home page - insight height set*/
        $('.insightContent .insTxtBox a').removeAttr('style');
		
		/*who we are - new insight height set*/
        $('.new_insightContent .insTxtBox .insInner').removeAttr('style');
		
		
		/**/
        $('.insightContent.rePropAdd .insTxtBox a').removeAttr('style');
		/*people media page quote*/
		$('.mdQuoteContent ul li div').removeAttr('style');
		
		/*people published page quote*/
		$('.publishBoxesContent .pubTxtBox a').removeAttr('style');
				
		/*Blog post height*/
		$('.insPostsArticle_1 .postsBoxes').removeAttr('style');
		$('.insPostsArticle_2 .postsBoxes').removeAttr('style');
		
        heightSet();
    });

    function heightSet() {
        var proHeight = 0;
		var homeInsHeight = 0;
		var new_InsHeight = 0;
        var insHeight = 0;
		var mdHeight = 0;
		var publishHeight = 0;
		var blogPostHieight = 0;
		var blogPostHieight_2 = 0;
		/*Home page Our program*/
        $('.homeProgram a.programBox').each(function () {
            if ($(this).height() > proHeight) {
                proHeight = $(this).height();
            }
        });
        $('.homeProgram a.programBox').height(proHeight);

		/*Home page - insight height set*/
		$('.insightContent .insTxtBox a').each(function () {
            if ($(this).height() > homeInsHeight) {
                homeInsHeight = $(this).height();
            }
        });
        $('.insightContent .insTxtBox a').height(homeInsHeight);
		
		
		/*who we are - new insight height set*/
		$('.new_insightContent .insTxtBox .insInner').each(function () {
            if ($(this).height() > new_InsHeight) {
                new_InsHeight = $(this).height();
            }
        });
        $('.new_insightContent .insTxtBox .insInner').height(new_InsHeight);
		
		
		/**/
        $('.insightContent.rePropAdd .insTxtBox a').each(function () {
            if ($(this).height() > insHeight) {
                insHeight = $(this).height();
            }
        });
        $('.insightContent.rePropAdd .insTxtBox a').height(insHeight);
		
		
		/*people media page quote*/
        $('.mdQuoteContent ul li div').each(function () {
            if ($(this).height() > mdHeight) {
                mdHeight = $(this).height();
            }
        });
        $('.mdQuoteContent ul li div').height(mdHeight);
		
		
		/*people Published page quote*/
        $('.publishBoxesContent .pubTxtBox.oneHalf a').each(function () {
            if ($(this).height() > publishHeight) {
                publishHeight = $(this).height();
            }
        });
        $('.publishBoxesContent .pubTxtBox.oneHalf a').height(publishHeight-90);
		
		
		/*Blog post height*/
        $('.insPostsArticle_1 .postsBoxes').each(function () {
            if ($(this).height() > blogPostHieight) {
                blogPostHieight = $(this).height();
            }
        });
        $('.insPostsArticle_1 .postsBoxes').height(blogPostHieight);
		
		
        $('.insPostsArticle_2 .postsBoxes').each(function () {
            if ($(this).height() > blogPostHieight_2) {
                blogPostHieight_2 = $(this).height();
            }
        });
        $('.insPostsArticle_2 .postsBoxes').height(blogPostHieight_2);
    }
	
});


	}(jQuery));
