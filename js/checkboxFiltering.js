 /* * *****************************************************************************
 *                JQUERY CODE FOR CHECKBOX FILTERING
 * ******************************************************************************
 *      Author:     Ramesh Kumawat
 *      Email:      ramesh.kumawat@edynamic.net
 *      Website:    http://www.edynamic.net
 * 		Date Created 08 July 2014
 *      File:       checkboxFiltering.js
 *      Version:    1.1
 *      Licensed:   Under MIT
 * ******************************************************************************
 *  VERION HISTORY:
 *                  NONE
 *
 * ******************************************************************************
 *@Use
 *        $('.optionChkFilters') .checkboxFiltering();
 *        $('input') .checkboxFiltering();
 *        $('input[type="checkbox"]') .checkboxFiltering();
 *		
 * 
 * *******************************************************************************/ 

 $.fn.checkboxFiltering = function(options){
	
	/**************people search filter***************/
	$('input[type="checkbox"]') .removeAttr('checked');
	/**** input filter code move to biolisting.js ****/
	
	
	 /*filter input click option*/
	 $(this).click(function(){
		checkFiltering(this.name, this, this.title);
	 });
	
	/*Service filter*/
	function checkCondition(ck_Name, ck_Id){
		var ck_Name = ck_Name;
		var ck_Id = ck_Id;
		var checkSerLength = $('input[name="'+ ck_Name +'"]:checked') .length;
		
		if(checkSerLength == 1){
			//$(ck_Id +' .filterSection') .show();
		}else if(checkSerLength == 0){
			$(ck_Id) .remove();
		}
	};
	
	function mainFilterBox(){
		if($('.searchFilterBox > div').size() == 0){
			$('.searchFilterBox').hide();
		}else{
			$('.searchFilterBox').show();
		}
	}
	
	function checkFiltering(name, self, title){
		//variable declaration
		var self = self;
		var ck_Name = name;
		var conTitle = title;
		var ck_Id = '#'+name;
		// filter box show hide
		checkCondition(ck_Name, ck_Id);
		
		//checked content show and unchecked content remove
		if ($(self).is(':checked')) {
			if ($(ck_Id).length == 0){
				$('.searchFilterBox') .append('<div id="'+ck_Name+'"><div class="filterSection"><a href="javascript:void(0);" class="allRemove" data-title="'+ck_Name+'">Clear All</a><h3>'+conTitle+'</h3><ul></ul></div></div>');
			}
			$(ck_Id + ' ul') .append('<li><em>'+ $(self).attr("alt")+ '</em><span class="removePartFil" data-title="'+ck_Name+'"><a href="javascript:void(0);">(x)</a></span></li>');
		}
		else {
			$(ck_Id +' li:contains('+ $(self).attr("alt") +')').remove();
		}
		mainFilterBox();
	};
	
	/*Removing function search filters*/
	$('.searchFilterBox') .on('click','.allRemove', function(){
		var title = $(this).attr('data-title');
		$('input[name="'+title+'"]').attr('checked',false);  
		$(this).next().next('ul').find('li') .remove();
		$(this).parent() .parent('#'+title).remove();
		mainFilterBox();
	 });
	 
	 $('.searchFilterBox') .on('click','span.removePartFil', function(){
		 var filLiText = $(this) .parent() .find('em') .text();
		 var title = $(this) .attr('data-title');
		 var parentId = '#'+$(this) .parentsUntil('.filterSection') .parent() .parent() .attr('id');
		 $('input[name="'+title+'"][alt="'+filLiText+'"]').attr('checked',false); 
		 $(this) .parent('li') .remove();
		 checkCondition(title, parentId);
			mainFilterBox();
	 });
	 
	 
	/*End people search filter*/
	
 };
